require 'test_helper'

class BaseControllerControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "Track Your Time | "
  end

  test "should get home" do
    get home_url
    assert_response :success
    assert_select "title", "#{@base_title}Home"
  end

  test "should get help" do
    get help_url
    assert_response :success
    assert_select "title", "#{@base_title}Help"
  end

  test "should get about" do
    get about_url
    assert_response :success
    assert_select "title", "#{@base_title}About"
  end

end
