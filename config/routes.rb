Rails.application.routes.draw do

  get '/home', to: 'base_controller#home'

  get '/help', to: 'base_controller#help'

  get '/about', to: 'base_controller#about'

  root 'base_controller#home'

  get '/signup', to: 'users#new'

  post '/signup',  to: 'users#create'

  get    '/login',   to: 'sessions#new'

  post   '/login',   to: 'sessions#create'

  delete '/logout',  to: 'sessions#destroy'

  get '/users/:id/changepassword(.:format)', to: 'users#changepassword', as: "user_changepassword"

  resources :users

end
