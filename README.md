# README

This will be the time logging application based on Rails and built using pure JS
and Rails MVC structures. It will utilize user authentication to store and access
project or work start and end times and do the calculations to correctly give you
an idea what you have worked or dedicated time to.
